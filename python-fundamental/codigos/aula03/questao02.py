'''
Questão02 - Lista03
2. Utilizando a lista A, do exemplo anterior, atribua a primeira metade da lista a para uma lista e
resultante B e a segunda metade para uma lista C. Feito isso, intercale os valores de B e C em uma
lista D final. Ex:
	A = [1,2,3,4,5,6]
	B = [1,2,3] e C = [4,5,6]
	D = [1,4,2,5,3,6]
'''

A = list(range(10)) # atribui uma lista para A

meio = len(A)//2 # guarda o valor do indice do meio

B = A[:meio] # atribui a primeira metade de A para lista B
C = A[meio:] # atribui a segunda metade de B para lista C

D = []

for i in range(meio):
	D.append(B[i])
	D.append(C[i])

if len(B) != len(C):
	D.append(C[-1])

# imprime as istas finais
print('A:',A)
print('B:',B)
print('C:',C)
print('D:',D)
