'''
Questão01 - Lista03
A partir de uma lista A de 100 números, obtenha outras duas listas B e C. A lista B deverá receber
apenas os valores ímpares de A, e a lista C os valores pares. Tome como partida o seguinte código
para sua lista A:
	A = list(range(0,100,3))
'''

A = list(range(0,100,3))
print('A:',A) # imprime a lista A

# inicia B e C com listas vazias
B = []
C = []

for num in A:
	if num%2 != 0: # testa se é ímpar
		B.append(num) # adiciona em B se é ímpar
	else:
		C.append(num) # adiciona em C se é par

# imprime os resultados
print('B:',B)
print('C:',C)
