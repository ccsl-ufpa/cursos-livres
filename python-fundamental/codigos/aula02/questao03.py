'''
Questão 03
Faça um programa que leia um nome completo
e exiba o nome no formato: ULTIMO, Primeiro.
'''
print('Manipulando nomes')
nome_completo = input('Nome completo: ')

nome_split = nome_completo.split() # sepra o nome com base nos espaços
primeiro = nome_split[0].title() # [0] primeira posição da lista
ultimo = nome_split[-1].upper() # [-1] última posição da lista

nome_formatado = ultimo+', '+primeiro

print('Formatado:',nome_formatado)

