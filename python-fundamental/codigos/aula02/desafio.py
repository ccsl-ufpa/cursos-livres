'''
Questão07 - Lista2
Dizemos que um número natural é triangular se ele é produto de três números naturais
consecutivos. Exemplo: 120 é triangular, pois 4.5.6 = 120. Dado um inteiro não-negativo n,
verificar se n é triangular.
'''

print('Informa se um numero é triangular:')
n_alvo = int(input('Informe um número: '))

n = 1 # n mínimo para que tenhamos (n-1)*n*(n+1)

produto = 0 # primeira atribuição. equivale a 0*1*2

while produto<n_alvo: # testa se o produto é igual ou maior que o número alvo
	n+=1 # novo valor de n
	produto = (n-1)*n*(n+1) # atribui produto para novo valor de n

# ao fim do laço, verifica se o último produto é igual ao alvo
if produto == n_alvo:
	print(n_alvo,'é triangular')
else:
	print(n_alvo,'não é triangular')
