'''
Questão02
Faça um programa que calcule o aumento de um salário. Ele deve
solicitar o valor do salário e a porcentagem do aumento. Exiba o
valor do aumento e do novo salário.
'''

print('Cálculo de aumento de salário')

salario = float(input('Informe o salário atual: '))

# ex: 10, 20 ou 30 equivale a 10%, 20% ou 30%
porcentagem = float(input('Porcentagem de aumento (%): '))/100

aumento = salario * porcentagem

novo_salario = salario + aumento

print('Salário com '+str(porcentagem*100)+'% de aumento:',novo_salario)
