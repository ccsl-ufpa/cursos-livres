'''
Questão01
Escreva um programa que leia um valor em
metros e o exiba convertido em milímetros.
'''

print('Converte metros em milímetros')

metros = input('Valor em metros: ')

milimetros = float(metros) * 1000

print(metros,'metro(s) equivale a',milimetros,'milimetro(s)')
