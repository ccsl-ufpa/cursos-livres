from xml.etree import ElementTree as et
from urllib import request as req

class Deputado:
    '''Classe Deputado'''
    def __init__(self,nome,sexo,uf,url_foto,partido,email):
        '''Construtor'''
        self.nome = nome
        self.sexo = sexo
        self.uf = uf
        self.url_foto = url_foto
        self.partido = partido
        self.email = email

    def __str__(self):
        '''Semlehante ao toString'''
        result = 'Nome: '+self.nome+'\n'
        result += 'Sexo: '+self.sexo+'\n'
        result += 'UF: '+self.uf+'\n'
        result += 'Foto: '+self.url_foto+'\n'
        result += 'Partido: '+self.partido+'\n'

        return result

def main():
    def getxml(url_xml):
        # obtenção do texto puro do url informado
        text_only = req.urlopen(url_xml).read()
        
        # transforma o texto puro em um ElementTree
        tree = et.fromstring( text_only )
        
        return tree
    
    xml = getxml('https://www.camara.leg.br/SitCamaraWS/Deputados.asmx/ObterDeputados')
    deputados = []
    
    tags = ['nome','sexo','uf','urlFoto','partido','email']
    for no in xml:
        args = []
        for tag in tags:
            args.append(no.find(tag).text) # selecionando os dados interessantes
        # instanciando um novo Deputado
        deputados.append(
            Deputado(args[0],args[1],args[2],args[3],args[4],args[5]))

    for deputado in deputados[:10]:
        print(deputado)
main()
