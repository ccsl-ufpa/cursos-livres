def abrir_arquivo(nome_arquivo):
	f = open(nome_arquivo)
	return f

def tratanome(nome):
	split = nome.split()
	return split[-1].upper()+', '+' '.join(split[:-1])

def tratadata(data):
	dia, mes, ano = data.split('/')
	print(mes)
	dic = {1:'Janeiro',2:'Fevereiro',8:'Agosto'}
	return dia+' de '+dic.get(int(mes))+' de '+ano

def main():
	texto_puro = abrir_arquivo('dados').read() # a partir do arquivo recebe o texto do arquivo
	linhas = texto_puro.splitlines() # separa cada linha como um elemmento string de uma lista
	# realiza atribuições para cada dado
	nome = tratanome(linhas[0])
	idade = linhas[1]
	data = tratadata(linhas[2])
	curso = linhas[3]

	print("Curso:",curso)
	print("Nome:",nome)
	print("Idade:",idade,'ano(s)')
	print("Data:",data)

main()
